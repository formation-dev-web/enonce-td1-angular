TD1 Angular − Découverte de TypeScript
======================================

Nous allons pratiquer **TypeScript**, le but des exercices est de *ré-écrire*
du JavaScript en utilisant les fonctionalités TypeScript. Tous les exercices
seront faits via
le [TypeScript Playground](https://www.typescriptlang.org/play/).

- **Pensez à faire une copie de votre code dans les fichiers `exo1.ts` et
`exo2.ts` une fois satisfait·e du résultat**.
- **Commitez après chaque question**

Exo 1
-----

Ré-écrire en typescript.

```js
var a = "Bonjour";
var b;

b = true;

var couleurs;

couleurs = ["bleu", "rouge", "jaune"];

for (var i = 0; i < couleurs.length; i++) {
    var couleur = couleurs[i];
    alert(couleur);
}
```

### BONUS

1. Utilisez `Array.forEach` pour parcourir le tableau (cela n'a rien de
spécifique à TS, c'est de l'ES5)
2. au *1.* vous avez utilisé (probablement) une fonction anonyme, ré-écrivez
cette fonction anonyme en utilisant
la
[notation « anonyme fléchée »](http://yahiko.developpez.com/tutoriels/introduction-typescript/#L5) offerte
avec TS.

Exo 2 − Feuille d'émargement
----------------------------

Nous allons créer une *classe* qui représente une feuille d'émargement. Nous
allons considérer qu'une feuille d'émargement contient :

- une date (sous forme de chaîne de caractères)
- un nom de module
- une durée (nombre de minutes)
- un nom d'intervenant
- une liste de présent·e·s

Chaque présent·e sera représenté·e par une unique chaîne de caractères contenant
son nom et son prénom.

1. Écrire la *classe* `FeuilleEmargement`, **dans ce premier temps sans gérer
   la liste de présent·e·s**;
2. *instancier* deux fois cette classe avec des paramètres différents ;
3. ajouter le support de la liste de présent·e·s ;
4. écrire une méthode `toString()` (aucun argument) à cette classe, qui
   retourne une chaîne de caractères au format suivant :

```
-------------------------------------
  MODULE ANGULAR, 12/12/2012 (3h30)
-------------------------------------
Intervenant : Jocelyn Delalande
Présent·e·s :
- Ursula le Gin
- John doe
- Jack Sparow
- Casimir
-------------------------------------
```

**NB : la méthode *retourne une chaîne*, elle ne l'*affiche* pas.**

4. Pour tester votre méthode, afficher (`alert()`) ce qu'elle retourne.

### BONUS

1. Créer une classe `Personne` à deux champs (`nom` et `prenom`) ;
2. Modifiez votre classe `FeuilleEmargement` pour que
    - le champ `intervenant` contienne une `Personne` au lieu d'une `string` ,
    - le champ `presents` contienne une liste de `Personne` au lieu d'une liste
      de `string`.
3. Implémentez `Personne.toString()` pour produire une chaîne, au format `Jocelyn
   DELALANDE`.
4. Modifiez `FeuilleEmargement.toString()` pour appeller `Personne.toString()`
   quand nécessaire.
5. Utiliser le type `Date` plutôt q'une chaîne de caractères pour la date.
6. Ré-écrivez `Personne` sous la forme d'une *Interface* plutôt que d'une classe.
